# LeSS intro
> Large-Scale Scrum (More with LeSS)

+++

## Two Agile Scaling Frameworks

* LeSS: Up to eight teams
* LeSS Huge: Up to a few thousand people on one product.

---

![LeSS Framework](https://less.works/img/framework/why-less-framework.png)

> LeSS is Scrum applied to many teams working together on one product.

+++

## LeSS Framework

* 1 Product Backlog
* 1 Definition of Done 
* 1 Potentially Shippable Product Increment
* 1 Product Owner
* 0 specialist team
* many cross-functional teams

--- 

## 1 Product Owner

* Prioritization over Clarification
* Ensuring the maximum return on investment (ROI)
* Focus outward on customers
* Bringing teams and customers together

+++

## PO attends to:

* Sprint Planning 1
* Overall Product Backlog Refinement
* Sprint Review
* Overall Retrospective

> Not to team-specific meetings (Sprint Planning 2, Daily ...)

+++

![PORelations](https://less.works/img/framework/product-owner-relationships.png)

---

## Sprint Planning 1

* PO and Teams (representatives)
* Select which Team works on which PBI
* Define Team Sprint Goals

---

## Sprint Planning 2

1. Each Team creates a plan for getting selected PBIs 'done'
1. Multi-Team Sprint Planning 2
    1. Related topics
    1. Influence of same component

> Sprint Backlog = single Scrum Team

+++

## Multi-Team Sprint Planning 2

* have a shared design session
* ask questions of one another at any time
* coordinate shared work
* find other opportunities to work together and learn from each other (e.g. cross-team pair programming).

---

![Sprint Planning](https://less.works/img/framework/sprint-planning.png.pagespeed.ce.Pp7bD-efNT.png)

---

## Coordination and Integration 
* Just Talk
* Communicate in Code
* Send Observers to the Daily Scrum
* Component communities and mentors
* Multi-team meetings
* Travelers ~~Silos~~
* Leading Team

+++

## Just Talk

* Discuss issue with other teams
* Get up and talk if coordination is needed
* Face to face > picking up the phone > dropping an email

> On VCS update: go over the changes and "just talk" if there is a need to synchronize.  

+++

## Communicate in Code

> continuous integration is a developer practise!

* to keep a working system
* by small changes
* growing the system
* by integrating at least daily
* on the mainline (not on a branch)
* supported by a CI system
* with lots of automated tests

+++

## Send Observers to the Daily Scrum

> Daily = single Scrum Team

**coordination**
* team members join to observe other teams
* Scrum Of Scrums is not recommended

+++

## Component communities and mentors

1 or N Component mentor(s)

* how does a component work (teacher)
* monitoring the long-term health  
* organizing design workshops
* reviewing code (not a gate)
 
+++

## Multi-team meetings

* Product Backlog Refinement
* Sprint Planning One / Two
* Design Workshops
* Exchange members (e.g. Daily Scrum)
* Overall Retrospective

+++

## Travelers ~~Silos~~

> Identify and break bottlenecks. Increase skills across teams. 

* Experts become travelers to spread knowledge
* Join a different team
* Coaching via pairing, workshops and teaching sessions

+++

## Leading Team

* Split "big" features into smaller PBIs
* One Leading Teams coordinate other teams
* Leading Team teaches other teams

---

## Scrum Master

* see the whole Product
* teach the organisation
* resolving scale problems
* encourage to find simple solutions
* create transparency

---

## Development Team

* work closely with customers/users 
* self-organizing
* cross-functional
* synchronize with other teams
* shares responsibility for all the team's work

---

## Product Backlog Refinement 

* Overall Product Backlog Refinement (shared amongst all teams)
* Team-level Product Backlog Refinement
* Multi-team Product Backlog Refinement

+++

![Product Backlog Refinement](https://less.works/img/framework/product-backlog-refinement.png)

---

## Overall Retrospective

* Explore improving the overall system
* Not focusing on one Team
* Systemic organizational issues?
* 45 minutes per week of Sprint, after the team Retro
* Product Owner, Scrum Masters, and rotating representatives from each Team